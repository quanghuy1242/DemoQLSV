﻿--PhongBan
CREATE PROCEDURE PhongBan_SelectAll
AS
BEGIN
	SELECT * FROM dbo.PhongBan
END

CREATE PROCEDURE PhongBan_Insert (@tenPB NVARCHAR(50))
AS
BEGIN
	INSERT INTO dbo.PhongBan(tenPB)
	VALUES(@tenPB)
END

CREATE PROCEDURE PhongBan_Update (
	@tenPB NVARCHAR(50),
	@Id INT
)
AS
BEGIN
	UPDATE dbo.PhongBan SET tenPB=@tenPB WHERE Id=@Id
END

CREATE PROCEDURE PhongBan_Delete (
	@Id INT
)
AS
BEGIN
	DELETE FROM dbo.PhongBan WHERE Id=@Id
END

--Nhan Vien
CREATE PROCEDURE NhanVien_SelectID (
	@Id INT
)
AS
BEGIN
	SELECT * FROM dbo.SinhVien WHERE Id=@Id
END

CREATE PROCEDURE NhanVien_Insert (
	@HoTen NVARCHAR(50),
	@QueQuan NVARCHAR(50),
	@Id INT
)
AS
BEGIN
	INSERT INTO dbo.SinhVien(HoTen, QueQuan, Id)
	VALUES(@HoTen, @QueQuan, @Id)
END

CREATE PROCEDURE NhanVien_Update (
	@HoTen NVARCHAR(50),
	@QueQuan NVARCHAR(50),
	@MaSV INT
)
AS
BEGIN
	UPDATE dbo.SinhVien SET HoTen=@HoTen, QueQuan=@QueQuan WHERE MaSV=@MaSV
END

CREATE PROCEDURE NhanVien_Delete (
	@MaSV INT
)
AS
BEGIN
	DELETE FROM dbo.SinhVien WHERE MaSV=@MaSV
END