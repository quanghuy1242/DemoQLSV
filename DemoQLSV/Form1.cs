﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DemoQLSV
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        //#region Kết Nối Cơ Sở Dữ Liệu bằng ADO.NET
        //SqlConnection conn = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=Demo;Integrated Security=True");
        //#endregion
        
        DemoEntities db = new DemoEntities();
        private void frmMain_Load(object sender, EventArgs e)
        {
            cbPhongBan.DisplayMember = "tenPB";
            cbPhongBan.ValueMember = "Id";
            cbPhongBan.DataSource = db.PhongBan_SelectAll();

            //Binding
            txtID.DataBindings.Clear();
            txtID.DataBindings.Add("Text", cbPhongBan.DataSource, "ID");

            txttenPB.DataBindings.Clear();
            txttenPB.DataBindings.Add("Text", cbPhongBan.DataSource, "tenPB");
        }

        private void cbPhongBan_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvNhanVien.DataSource = db.NhanVien_SelectID(Convert.ToInt32(cbPhongBan.SelectedValue));

            //Binding
            txtMaNV.DataBindings.Clear();
            txtMaNV.DataBindings.Add("Text", dgvNhanVien.DataSource, "MaSV");

            txtHoTen.DataBindings.Clear();
            txtHoTen.DataBindings.Add("Text", dgvNhanVien.DataSource, "HoTen");

            txtquequan.DataBindings.Clear();
            txtquequan.DataBindings.Add("Text", dgvNhanVien.DataSource, "QueQuan");

        }

        Boolean addPB = false;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            txttenPB.Text = "";
            txttenPB.Focus();
            addPB = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (addPB == true)
            {
                try
                {
                    db.PhongBan_Insert(txttenPB.Text);
                    frmMain_Load(sender, e);
                    MessageBox.Show("Thêm dữ liệu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Thêm dữ liệu thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                addPB = false;
            }
            else
            {
                try
                {
                    db.PhongBan_Update(txttenPB.Text, Convert.ToInt32(cbPhongBan.SelectedValue));
                    frmMain_Load(sender, e);
                    MessageBox.Show("Sửa dữ liệu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Sửa dữ liệu thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult lenh = MessageBox.Show("Bạn có chắc chắn xoá " + cbPhongBan.Text + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (lenh == DialogResult.Yes)
            {
                try
                {
                    db.PhongBan_Delete(Convert.ToInt32(cbPhongBan.SelectedValue));
                    frmMain_Load(sender, e);
                    MessageBox.Show("Xoá dữ liệu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Xoá dữ liệu thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        Boolean AddNV = false;
        private void btnAddNV_Click(object sender, EventArgs e)
        {
            txtMaNV.Text = "";
            txtHoTen.Text = "";
            txtquequan.Text = "";
            txtHoTen.Focus();
            AddNV = true;
        }

        private void btnUpdateNV_Click(object sender, EventArgs e)
        {
            if (AddNV == true)
            {
                try
                {
                    db.NhanVien_Insert(txtHoTen.Text, txtquequan.Text, Convert.ToInt32(txtID.Text));
                    cbPhongBan_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Thêm nhân viên thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Thêm nhân viên thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                AddNV = false;
            }
            else
            {
                try
                {
                    db.NhanVien_Update(txtHoTen.Text, txtquequan.Text, Convert.ToInt32(txtMaNV.Text));
                    cbPhongBan_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Thêm nhân viên Thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Sửa nhân viên thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnDeleteNV_Click(object sender, EventArgs e)
        {
            DialogResult lenh = MessageBox.Show("Bạn có chắc chắn xoá " + txtHoTen.Text + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (lenh == DialogResult.Yes)
            {
                try
                {
                    db.NhanVien_Delete(Convert.ToInt32(txtMaNV.Text));
                    cbPhongBan_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Xoá dữ liệu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch
                {
                    MessageBox.Show("Xoá dữ liệu thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
